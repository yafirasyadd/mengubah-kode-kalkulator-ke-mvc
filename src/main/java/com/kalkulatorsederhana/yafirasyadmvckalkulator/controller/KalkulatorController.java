package com.kalkulatorsederhana.yafirasyadmvckalkulator.controller;

import com.kalkulatorsederhana.yafirasyadmvckalkulator.service.KalkulatorService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kalkulatorsederhana.yafirasyadmvckalkulator.model.Number;

@Controller
@RequestMapping("")
public class KalkulatorController {

    private KalkulatorService kalkulatorService;

    @RequestMapping("")
    public String result(Number number, Model model ){
        String result="";

        int num1 = number.getNum1();
        int num2 = number.getNum2();
        String operator = number.getOperator();

        if (operator != null) {
            result = "Hasil dari "+ num1 + " " + operator + " " + num2 + " = ";
            result += kalkulatorService.kalkulator(num1, num2, operator);
            model.addAttribute("result", result);
        }
        return "view/index";

    }

}
