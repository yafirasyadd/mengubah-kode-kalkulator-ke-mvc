package com.kalkulatorsederhana.yafirasyadmvckalkulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YafirasyadMvcKalkulatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(YafirasyadMvcKalkulatorApplication.class, args);
	}

}
