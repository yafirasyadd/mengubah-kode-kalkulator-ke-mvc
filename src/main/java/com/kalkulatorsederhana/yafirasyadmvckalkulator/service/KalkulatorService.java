package com.kalkulatorsederhana.yafirasyadmvckalkulator.service;

import java.text.DecimalFormat;

public class KalkulatorService {

    public static String kalkulator(int num1, int num2, String operator ) {
        
         double result   = 0;
         
        if (operator.equalsIgnoreCase("+")) {
            result = num1 + num2;
        } else if (operator.equalsIgnoreCase("-")) {
            result = num1 - num2;
        } else if (operator.equalsIgnoreCase("X")) {
            result = num1 * num2;
        } else if (operator.equalsIgnoreCase("/")) {
            if (num2 == 0) {
                return "tidak bisa dilakukan";
            }
            result = num1 / num2;
        }

        DecimalFormat df = new DecimalFormat("#.##");
        return  df.format(result);

    }
}
